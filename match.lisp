(in-package :calculus)

(defgeneric match
  (pattern expr &optional dict banned))

(defmethod match
  (pattern expr &optional dict banned)
  (declare (ignore dict))
  (equalp pattern expr))

(defmethod match
  ((pattern any) expr &optional (dict (equalp-hash-table)) banned)
  "Matches expression to pattern of type 'any' any does not care about 
banned elements inside of it."
  (declare
    (ignore banned))
  (let ((matched (gethash pattern dict)))
    (cond ((and matched (equalp matched expr)) dict)
      ((not matched) (progn (setf (gethash pattern dict) expr) dict))
      (t nil))))

(defmethod match
  ((pattern var) expr &optional (dict (equalp-hash-table)) banned)
  "Matches expression to pattern of type 'var'. This match checks if 
pattern has banned symbols inside."
  (let ((matched (gethash pattern dict)))
    (cond ((and matched (equalp matched expr)) dict)
      ((and (not (recursive-search expr banned))(not matched))
        (progn (setf (gethash pattern dict) expr) dict))
      (t nil))))

(defmethod match
  ((pattern list) (expr list) &optional (dict (equalp-hash-table)) banned)
  (loop for (pattern-elem . guard) on pattern
    for expr-elem on expr
    if (and guard (null (cdr expr-elem)))
    return nil
    unless (if (and (not guard) (cdr expr-elem))
             (match pattern-elem (cons (car expr) expr-elem) dict banned)
             (match pattern-elem (car expr-elem) dict banned))
    return nil
    finally (return dict)))

(defmethod match
  ((pattern d) (expr d) &optional (dict (equalp-hash-table)) banned)
  (declare
    (ignore banned))
  (progn
    (match (d-d pattern) (d-d expr) dict)
    (match (d-f pattern) (d-f expr) dict (d-d expr))))

(defmethod match
  ((pattern i) (expr i) &optional (dict (equalp-hash-table)) banned)
  (declare
    (ignore banned))
  (progn
    (match (i-d pattern) (i-d expr) dict)
    (match (i-f pattern) (i-f expr) dict (i-d expr))))

(defgeneric translate
  (expr dict))

(defmethod translate
  (expr dict)
  expr)

(defmethod translate
  ((expr any) dict)
  (gethash expr dict))

(defmethod translate
  ((expr var) dict)
  (gethash expr dict))

(defmethod translate
  ((expr list) dict)
  (loop for elem in expr
    collect (translate elem dict)))

(defmethod translate
  ((expr d) dict)
  (with-slots (f d) expr
    (let* ((new-d (translate d dict))
            (new-f (translate f dict)))
      (d new-f new-d))))

(defmethod translate
  ((expr i) dict)
  (with-slots (f d) expr
    (let* ((new-d (translate d dict))
            (new-f (translate f dict)))
      (i new-f new-d))))
