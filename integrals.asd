(in-package :asdf)

(defsystem "calculus"
  :serial t
  :components ((:file "package")
	        (:file "types")
                (:file "reader")
                (:file "util")
                (:file "simplify")
                (:file "rules")
                (:file "match")
                (:file "solve")))

