(in-package :cl)

(defpackage :calculus
  (:nicknames :całki)
  (:use :cl)
  (:export
    :integral
    :derivative
    ))


(defpackage :calculus-user
  (:nicknames :całki-user)
  (:use :cl
    :calculus))

