Program do całkowania
=====================

Uruchomienie:
1. Skompilować integrals.asd
2. Załadować quicklispem

W pliku tests.lisp jest przykład tego co może policzyć,  
jest też pewna obsługa całkowania przez części, ale uprasczanie  
wyrażeń nie jest zbyt dobre i rozwiązania robią się ogromne,  
trudne do weryfikacji i jeszcze trudniejsze do zdebugowania.  
Program całkiem nieźle liczy za to pochodne.

Wewnętrznie do parsowania reguł używane są dwie struktury pomocnicze: ^ oraz $  
^x może być czymkolwiek
$x może być zmienną, która nie jest równa dziedzinie całkowania, chyba że dziedziną jest x,  
tzn. $x może być x, ale nie może być y.

Dla użytkownika przewidziane są makra integral i derivative.

