(in-package :calculus)

(defparameter +dollar-sign+ #\$)
(defparameter +hat+ #\^)

(defun read-dollar-sign (stream char)
  (declare
    (ignore char))
  (var (read stream)))

(defun read-hat (stream char)
  (declare
    (ignore char))
  (any (read stream)))
    
(set-macro-character +dollar-sign+ #'read-dollar-sign)
(set-macro-character +hat+ #'read-hat)
