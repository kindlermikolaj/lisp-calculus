(in-package :calculus)

(defparameter *differentiation-ruleset*
  (list
    ;; dx / dx = 1    
    (rule (d $x $x) 1)
    ;; dy / dx = 0
    (rule (d $y $x) 0)
    ;;additive rules
    (rule (d `(+ ,^f ,^g) $x)
      `(+ ,(d ^f $x) ,(d ^g $x)))
    (rule (d `(- ,^f ,^g) $x)
      `(- ,(d ^f $x) ,(d ^g $x)))    
    ;; multiplicative rule
    (rule (d `(*  ,^f ,^g) $x)
      `(+ (* ,(d ^f $x) ,^g) (* ,^f ,(d ^g $x))))
    ;; natural logarithm
    (rule (d `(log ,$x) $x)
      `(expt ,$x -1))
    (rule (d `(log ,^f) $x)
      `(* (expt ,^f -1) ,(d ^f $x)))
    ;; d(x^n) / dx = nx^*(n-1)
    (rule (d `(expt ,$x ,^n) $x)
      `(* ,^n (expt ,$x (- ,^n 1))))
    (rule (d `(/ ^f ^g) $x)
      `(/ (- (* ,(d ^f $x) ,^g) (* ,(d ^g $x) ,^f)) (expt ,^g 2)))
    ;; trigonometric rules 
    (rule (d `(sin ,^f) $x)
      `(* (cos ,^f) ,(d ^f $x)))
    (rule (d `(cos ,^f) $x)
      `(* (- sin ,^f) ,(d ^f $x)))
    ))


(defparameter *integration-ruleset*
  (list
    (rule (i `(expt ,$x ,^n) $x)
      `(/ (expt ,$x (+ ,^n 1)) (+ ,^n 1)))
    ;; trigonometric integrals
    (rule (i `(sin ,$x) $x)
      `(- (cos ,$x)))
    (rule (i `(cos ,$x) $x)
      `(sin ,$x))    
    (rule (i $x $x)
      `(* 1/2 (expt $x 2)))
    (rule (i $y $x)
      `(* ,$y ,$x))
    ;; x^(-1)
    (rule (i `(expt ,$x -1) $x)
      `(log ,$x))
    ;; additive rules
    (rule (i `(+ ,^f ,^g) $x)
      `(+ ,(i ^f $x) ,(i ^g $x)))
   (rule (i `(- ,^f ,^g) $x)
     `(- ,(i ^f $x) ,(i ^g $x)))
    ;; negation
   (rule (i `(- ,^f) $x)
      `(- ,(i ^f $x)))
    (rule (i `(* ,$y ,^f) $x)
      `(* ,$y ,(i ^f $x)))
    (rule (i `(* ,^f ,$y) $x)
      `(* ,$y ,(i ^f $x)))
    (rule (i `(/ ,^f ,$y) $x)
      `(/ ,(i ^f $x) ,$y))
    ;; integration by parts
    (rule (i `(* ,^f ,^g) $x)
      `(- (* ,(i ^f $x) ,^g)
         ,(i `(* ,(i ^f $x) ,(d ^g $x)) $x)))))


