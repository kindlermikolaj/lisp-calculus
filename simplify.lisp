(in-package :calculus)

(defgeneric simplify-impl (expr))

(defun simplify (expr)
  (let ((new (simplify-impl expr)))
    (if (equalp expr new)
      expr
      (simplify-impl new))))

(defmethod simplify-impl (expr) expr)

(defun simplify-multi-elems (op expr &optional (multi-expr #'list))
  (let ((counts (equalp-hash-table)))
    (flet ((update-count (expr)
             (let ((count (or (gethash expr counts) 0)))
               (setf (gethash expr counts) (+ count 1)))))
      (loop for elem in expr
        do (update-count elem))
      (loop for k being the hash-keys in counts using (hash-value v)
        collect (if (= v 1) k (funcall multi-expr op k v))))))

(defun simplify-opposing-pairs (op-pair expr)
  (destructuring-bind (agreeing-op . opposing-op) op-pair
    (let* ((lists (filter #'listp (cdr expr)))
            (atoms (filter #'atom (cdr expr)))
            (opposing (filter (op-eq opposing-op) lists))
            (agreed (filter (op-neq opposing-op) lists))
            (firsts (map 'list 'cadr opposing))
            (rests (apply #'append (map 'list 'cddr opposing))))
      `(,opposing-op ,(concatenate 'list (cons agreeing-op atoms) firsts agreed)
         ,(cons agreeing-op rests)))))

(defun simplify-associative (op op-0 op-nested-simplify list)
  (let*((simplified (map 'list #'simplify (cdr list)))
         (lists (filter #'listp simplified))
         (atoms (filter #'atom simplified))
         (appendable (map 'list #'cdr (filter (lambda (l) (eq (car l) op)) lists)))
         (not-appendable (filter (lambda (l) (not (eq (car l) op))) lists))
         (appended (apply #'append atoms appendable))
         (numeric (numeric-for-op op appended))
         (numeric-final (if (= numeric op-0) nil (list numeric)))
         (abstract (concatenate 'list
                     (filter (lambda (e) (not (numberp e))) appended)
                     not-appendable))
         (final (simplify (funcall op-nested-simplify 
                            (append numeric-final abstract)))))
    (if (> (length final) 1)
      (if (and (eq op '*) (= numeric 0)) 0 (cons op final))
      (car final))))

(defun simplify-multi+ (expr)
  (simplify-multi-elems '* expr))

(defun simplify-multi* (expr)
  (simplify-multi-elems 'expt expr))

(defun simplify-opposing+ (expr)
  (simplify-opposing-pairs '(+ . -) expr))

(defun simplify-opposing* (expr)
  (simplify-opposing-pairs '(* . /) expr))

(defun simplify+ (list)
  (simplify-associative '+ 0 #'simplify-multi+ list))

(defun simplify* (list)
  (simplify-associative '* 1 #'simplify-multi* list))

(defun simplify- (list)
  (if (= (length list) 2)
    (list '- (simplify (cadr list)))
    (if (= (length list) 3)
      (try-apply '- (simplify (cadr list))
        (simplify (caddr list)))
      (let ((simplified (simplify (cons '+ (cddr list))))
             (simplified-first (if (listp (cadr list))
                                 (simplify (cadr list))
                                 (cadr list))))
        (if (listp simplified)
          (cons '- (cons simplified-first (cdr simplified)))
          (try-apply '- simplified-first simplified))))))

(defun simplify/ (list)
  (if (= (length list) 2)
    (simplify (cadr list))
  (if (= (length list) 3)
    (try-apply '/ (simplify (cadr list))
      (simplify (caddr list)))
    (let ((simplified (simplify (cons '* (cddr list))))
           (simplified-first (if (listp (cadr list))
                               (simplify (cadr list))
                               (cadr list))))
      (if (listp simplified)
        (cons '/ (cons (cadr list) (cdr simplified)))
        (try-apply '/ simplified-first simplified))))))

(defun simplify-expt (list)
  (destructuring-bind (x p . nil) (cdr list)
    (let ((new-x (simplify x))
           (new-p (simplify p)))
      (if (and (listp new-x)
            (eq (car new-x) 'expt))
        `(expt ,(cadr new-x) ,(simplify `(* ,(caddr new-x) ,new-p)))
        (list 'expt new-x new-p)))))

(defmethod simplify-impl ((input list))
  (let ((op (car input)))
    (cond ((eq op '+) (simplify+ input))
      ((eq op '*) (simplify* input))
      ((eq op '/) (simplify/ input))
      ((eq op '-) (simplify- input))
      ((eq op 'expt) (simplify-expt input))
      (t (cons op (map 'list #'simplify (cdr input)))))))

(defmethod simplify-impl ((input d))
  (let ((new-f (simplify (d-f input))))
    (if (equalp 0 new-f) 0
      (d new-f (d-d input)))))

(defmethod simplify-impl ((input i))
  (let ((new-f (simplify (i-f input))))
    (if (equalp 0 new-f) ^c
      (i new-f (i-d input)))))


