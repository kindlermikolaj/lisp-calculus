(in-package :calculus)

(defun match-translate (expr rule)
  (with-slots (from to) rule
    (let  ((dict (match from expr)))
      (if dict
        (progn
          (translate to dict))
        nil))))

(defmethod solve (expr)
  (cond ((null expr) nil)
    ((listp expr)
      (cons (solve (car expr))
        (solve (cdr expr))))
    (t expr)))

(defmethod solve ((expr list))
  (simplify (loop for elem in expr
              collect (solve elem))))

(defmethod solve (expr) expr)

(defmethod solve ((expr d))
  (let* ((matched (map 'list (lambda (r) (match-translate expr r))
                    *differentiation-ruleset*))
          (translated (remove nil matched))
          (result (loop for elem in translated
                    with solution
                    do
                    (setf solution (solve elem))      
                    if solution
                    do (return (simplify solution)))))
    (or result expr)))


(defmethod solve ((expr i))
  (setf (i-f expr) (solve (i-f expr)))
  (print expr)
  (let* ((matched (map 'list (lambda (r) (match-translate expr r))
                    *integration-ruleset*))
          (translated (remove nil matched))
          (result (loop for elem in translated
                    with solution
                    do
                    (setf solution (solve elem))
                    if solution
                    do
                    (return (simplify solution)))))
    (or result expr)))

