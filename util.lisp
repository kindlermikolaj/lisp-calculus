(in-package :calculus)

(defun print-hash-table (table)
  (loop for k being the hash-keys in table
    using (hash-value v)
    do (print (cons k v))))

(defun equalp-hash-table ()
  (make-hash-table :test #'equalp))

(defun filter (f list)
  (loop for elem in list
    when (funcall f elem) collect elem))

(defun try-apply (op first rest)
  (if (and (numberp first)
        (numberp rest))
    (funcall op first rest)
    (list op first rest)))

(defun op-eq (op)
  (lambda (l) (eq op (car l))))

(defun op-neq (op)
  (lambda (l) (not (eq op (car l)))))

(defgeneric recursive-search (pattern banned))

(defmethod recursive-search (expr banned)
  (equalp expr banned))

(defmethod recursive-search ((expr list) banned)
  (loop for elem in expr
    if (recursive-search elem banned)
    do (return t)
    finally (return nil)))

(defun numeric-for-op (op list)
  (let ((numeric-list (filter 'numberp list)))
    (cond ((find op '(/ *)) (apply #'* numeric-list))
      ((find op '(+ -)) (apply #'+ numeric-list))
      (t (apply op numeric-list)))))
