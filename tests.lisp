(in-package :calculus)

(equalp 'g
  (derivative (* (+ 2 x) g) x))
(eq 1
  (derivative (+ 2 x) x))

(eq 0
  (derivative (* 2 t y) x))

(equalp '(* 2 x y)
  (derivative (* x x y) x))

;; (d(sinx * cosx) / dx = cos^2x - sin^2x)
(equalp '(+ (expt (cos X) 2) (* (sin X) (- sin X)))
  (derivative (* (sin x) (cos x)) x))

(equalp '(* t (expt x 2))
  (integral (* 2 t x) x))

(equalp '(+ (- (cos x)) (sin x))
  (integral (+ (sin x) (cos x)) x))

(equalp '(* 2 (expt x 2))
  (solve (i `(* 2 ,(d '(* x x) 'x)) 'x)))

(equalp '(* 1/2 (expt x 2) (sin y))
  (integral (* (sin y) x) x))

(equalp '(/ (expt x 12) 12)
  (integral (expt x 11) x))

(equalp '(* 1/2 a b c d (expt z y) (expt x 2))
  (integral (* (* a b c d (expt z y)) x) x))

(equalp '(* 14 (/ (expt x 3) 3))
  (integral (* 1 2 (+ 3 4) x x) x))

;; przez części

;; to jest dobre - sprawdziłem
(equalp '(- (* (+ (* 2 X) (* 1/2 (expt X 2))) (+ 3 X))
           (+ (expt X 2) (* 1/2 (/ (expt X 3) 3))))
  (integral (* (+ x 2) (+ x 3)) x))

;; te nie wiem, za dużo nawiasów żeby zweryfikować
(integral (* x (sin x)) x)
(integral (* x (cos x)) x)


;; to jest na pewno źle policzone
(integral (* (sin x) (cos x)) x)
