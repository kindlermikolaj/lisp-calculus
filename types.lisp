(in-package :calculus)

(defstruct d f d)
(defstruct i f d)
(defstruct var symbol)
(defstruct any symbol)

(defclass rule ()
  ((from :initarg :f
     :accessor rule-from)
    (to :initarg :t
      :accessor rule-to)))

(defgeneric solve (problem)
  (:documentation "Run adequate solving algorithm for given problem."))

(defgeneric translate (dict expr)
  (:documentation "Replace 'variables' with expr."))

(defmacro integral (f d)
  `(solve (i ',f ',d)))

(defmacro derivative (f d)
  `(solve (d ',f ',d)))

(defmethod print-object ((obj d) stream)
  (with-slots (f d) obj
    (format stream "<d~a /d~a>" f d)))

(defmethod print-object ((obj i) stream)
  (with-slots (f d) obj
    (format stream "<∫ ~a d~a>" f d)))

(defmethod print-object ((obj rule) stream)
  (with-slots (from to) obj
    (format stream "[~a = ~a]" from to)))

(defmethod print-object ((obj var) stream)
  (format stream "$~a" (var-symbol obj)))

(defmethod print-object ((obj any) stream)
  (format stream "^~a" (any-symbol obj)))

(defun d (f d)
  (make-d :f f :d d))

(defun i (f d)
  (make-i :f f :d d))

(defun var (symbol)
  (make-var :symbol symbol))

(defun any (symbol)
  (make-any :symbol symbol))

(defun rule (from to)
  (make-instance 'rule :f from :t to))


(defmethod make-load-form ((var var) &optional env)
  (declare (ignore env))
  (make-load-form-saving-slots var))

(defmethod make-load-form ((any any) &optional env)
  (declare (ignore env))
  (make-load-form-saving-slots any))

